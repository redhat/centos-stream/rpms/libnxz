# libnxz

[libnxz](https://github.com/libnxz/power-gzip) is a zlib-compatible library
that uses the NX GZIP Engine available on POWER9 or newer processors in order
to provide a faster zlib/gzip compression without using the general-purpose
cores.
